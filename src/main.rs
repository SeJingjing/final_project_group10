use actix_web::{get, web, App, HttpServer, Responder, HttpResponse};
use onnxruntime::{environment::Environment, tensor::OrtOwnedTensor};
use serde::{Deserialize, Serialize};
use tokenizers::tokenizer::{Result, Tokenizer};
use tokenizers::Encoding;
use ndarray::Array2;


#[derive(Deserialize)]
struct InputData {
    text: String,
}

#[derive(Serialize)]
struct OutputData {
    label: String,
    probability: f32,
    probabilities: Vec<f32>,
}

fn softmax(logits: &[f32]) -> Vec<f32> {
    let max_logit = logits.iter().fold(f32::MIN, |max, &val| max.max(val));
    let exps: Vec<f32> = logits.iter().map(|&val| (val - max_logit).exp()).collect();
    let sum_exps: f32 = exps.iter().sum();
    exps.iter().map(|&val| val / sum_exps).collect()
}


#[get("/")]
async fn analyze(data: web::Query<InputData>) -> impl Responder {
    let environment = Environment::builder().with_name("onnx-inference").build().unwrap();

    let encoding = tokenize_input(&data.text).unwrap(); 

    let mut session = environment.new_session_builder().unwrap()
                     .with_model_from_file("torch-model.onnx").unwrap();

    let input_ids: Array2<i64> = Array2::from_shape_vec((1, encoding.get_ids().len()), 
        encoding.get_ids().iter().map(|&id| id as i64).collect()).unwrap();
    let attention_mask: Array2<i64> = Array2::from_shape_vec((1, encoding.get_attention_mask().len()), 
        encoding.get_attention_mask().iter().map(|&mask| mask as i64).collect()).unwrap();


    let outputs: Vec<OrtOwnedTensor<f32, _>> =
        session.run(vec![input_ids.into(), attention_mask.into()]).unwrap();

    let logits: Vec<f32> = outputs[0].iter().cloned().collect();
    let probabilities: Vec<f32> = softmax(&logits);

    let (label, probability) = if probabilities[0] > probabilities[1] {
        ("Negative".to_string(), probabilities[0])
    } else {
        ("Positive".to_string(), probabilities[1])
    };

    let response = OutputData {
        label,
        probability,
        probabilities,
    };
    HttpResponse::Ok().json(response)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(analyze)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}

fn tokenize_input(input: &str) -> Result<Encoding> {
    let tokenizer = Tokenizer::from_pretrained("bert-base-uncased", None)?;    
    let encoding = tokenizer.encode(input, false)?;
    
    Ok(encoding)
}
