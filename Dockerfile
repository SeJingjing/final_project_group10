# Use the official Rust image for the build stage
FROM rust:bookworm as builder

# Create a new empty shell project
RUN USER=root cargo new --bin final_web
WORKDIR /final_web

# Copy over your manifests
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

# Cache your dependencies (this is a trick to avoid rebuilding dependencies if unchanged)
RUN cargo build --release
RUN rm src/*.rs

# Copy your source tree
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/final_web*
RUN cargo build --release

# Use Debian Bullseye for the runtime base image
FROM debian:bookworm-slim

# Install necessary packages for ONNX and Actix runtime
RUN apt-get update && apt-get install -y \
    ca-certificates \
    libssl-dev \
    libc6-dev \
    libgomp1 \
    && rm -rf /var/lib/apt/lists/*

# Install ONNX Runtime
RUN apt-get update && apt-get install -y wget
RUN wget https://github.com/microsoft/onnxruntime/releases/download/v1.8.1/onnxruntime-linux-x64-1.8.1.tgz
RUN tar -zxvf onnxruntime-linux-x64-1.8.1.tgz -C /usr/local/lib
RUN echo "/usr/local/lib/onnxruntime-linux-x64-1.8.1/lib" > /etc/ld.so.conf.d/onnxruntime.conf
RUN ldconfig

# Copy the build artifact from the build stage
COPY --from=builder /final_web/target/release/final_web .

# Copy model file
COPY ./torch-model.onnx ./

# Set the CMD to your server
CMD ["./final_web"]
